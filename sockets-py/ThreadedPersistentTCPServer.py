import socket
import threading

class ThreadedPersistentTCPServer:

    def handle_single_client(self, connection_socket):
        with connection_socket:
            try:
                # Read and write to the client
                in_from_client = connection_socket.makefile('r')
                out_to_client = connection_socket.makefile('w')
                
                while True:
                    # FIXME: Should check that we receive a complete sentence
                    # Read from the client
                    client_sentence = in_from_client.readline().strip()
                    
                    # If message is empty or a period, close the connection
                    if client_sentence == '':
                        break
                    print(f"Received: {client_sentence}")
                    if client_sentence == ".":
                        break
                    
                    # Capitalize the sentense
                    capitalized_sentence = client_sentence.upper() + '\n'
                    
                    # Write to the client
                    out_to_client.write(capitalized_sentence)
                    out_to_client.flush()
            finally:
                print(f"Closing client connection from {connection_socket.getpeername()}")
    
    def start_server(self):
        # Create the socket with localhost and port 6789
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as welcome_socket:
            welcome_socket.bind(('localhost', 6789))
            welcome_socket.listen()
            
            while True:
                try:
                    connection_socket, _ = welcome_socket.accept()
                    # Create a socket for this connection, calling handle single client function
                    threading.Thread(target=self.handle_single_client, args=(connection_socket,)).start()
                except Exception as e:
                    print(f"Error: {e}")
                    break

if __name__ == "__main__":
    server = ThreadedPersistentTCPServer()
    server.start_server()
