import socket

class PersistentTCPServer:
    
    # Handles communication with a single connected client
    def handle_single_client(self, connection_socket):
        with connection_socket:
            # Echo messages to make sure connection is established
            print(f"Connection established with {connection_socket.getpeername()}")
            while True:
                # FIXME: Should check that we receive a complete sentence
                # Receive data from the client
                client_sentence = connection_socket.recv(1024).decode('utf-8')
                if client_sentence:
                    print("Received:", client_sentence.strip())
                    # Check for quit signal
                    if client_sentence.strip() == ".":
                        break
                    # Send back the capitalized version of the received message
                    capitalized_sentence = client_sentence.upper()
                    connection_socket.sendall(capitalized_sentence.encode('utf-8'))
                else:
                    break
            print("Closing client connection from", connection_socket.getpeername())

    # Sets up and starts the server to listen for incoming connections
    def start_server(self):
        server_address = ('localhost', 6789)
        # Create the socket and start listening
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as welcome_socket:
            welcome_socket.bind(server_address)
            welcome_socket.listen()
            print("Server is listening...")

            # Keeps receiving messages
            while True:
                try:
                    # Accept incoming client connections and handle them
                    connection_socket, _ = welcome_socket.accept()
                    self.handle_single_client(connection_socket)
                except Exception as e:
                    print("An error occurred:", e)
                    break

# Instantiate and start the server
server = PersistentTCPServer()
server.start_server()
