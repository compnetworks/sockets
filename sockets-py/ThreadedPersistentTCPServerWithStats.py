import socket
import threading

class ThreadedPersistentTCPServerWithStats:

    class ClientStatistics:
        
        # Track the number of current and total clients
        def __init__(self):
            self.num_current_clients = 0
            self.num_total_clients = 0
            # Since multiple threads are sharing two common variables, lock is needed to prevent racing condition
            self.lock = threading.Lock()

        # Increment the current and total client counts
        def incr_num_current_clients(self):
            with self.lock:
                self.num_current_clients += 1
                self.num_total_clients += 1

        # Decrement the current client count
        def decr_num_current_clients(self):
            with self.lock:
                self.num_current_clients -= 1

        # Return the current client count
        def get_num_current_clients(self):
            with self.lock:
                return self.num_current_clients

        # Return the total client count
        def get_num_total_clients(self):
            with self.lock:
                return self.num_total_clients

    class SingleClientHandler(threading.Thread):
        # Initialize the thread with the client socket and statistics
        def __init__(self, connection_socket, c_stats):
            # inherit from threading.Thread
            super().__init__()
            self.connection_socket = connection_socket
            self.c_stats = c_stats

        # Handle the client connection in a separate thread
        def run(self):
            try:
                self.handle_single_client(self.connection_socket)
            except Exception as e:
                print(f"Error handling client: {e}")
            finally:
                # Update statistics and close the socket when done
                try:
                    self.c_stats.decr_num_current_clients()
                    print(f"#current_clients = {self.c_stats.get_num_current_clients()}")
                    print(f"#total_clients = {self.c_stats.get_num_total_clients()}")
                    self.connection_socket.close()
                except Exception as e:
                    print(f"Error closing socket: {e}")

        # Process messages from a single client
        def handle_single_client(self, connection_socket):
            with connection_socket:
                in_from_client = connection_socket.makefile('r')
                out_to_client = connection_socket.makefile('w')
                
                # Read and process client input
                while True:
                    client_sentence = in_from_client.readline().strip()
                    if client_sentence == '':
                        break
                    print(f"Received: {client_sentence}")
                    if client_sentence == ".":
                        break
                    capitalized_sentence = client_sentence.upper() + '\n'
                    out_to_client.write(capitalized_sentence)
                    out_to_client.flush()

    # Start the server and manage client connections
    def start_server(self):
        c_stats = self.ClientStatistics()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as welcome_socket:
            welcome_socket.bind(('localhost', 6789))
            welcome_socket.listen()
            
            try:
                while True:
                    # Accept new client connections and start a new thread for each
                    connection_socket, _ = welcome_socket.accept()
                    
                    # Create a thread for each connection socket
                    handler = self.SingleClientHandler(connection_socket, c_stats)
                    handler.start()
                    c_stats.incr_num_current_clients()
                    print(f"#current_clients = {c_stats.get_num_current_clients()}")
                    print(f"#total_clients = {c_stats.get_num_total_clients()}")
            except Exception as e:
                print(f"Error in server: {e}")

# Instantiate and start the server
server = ThreadedPersistentTCPServerWithStats()
server.start_server()
