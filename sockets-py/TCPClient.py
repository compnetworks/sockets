import socket

serverName = 'localhost'
serverPort = 6789
clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket.connect((serverName, serverPort))
sentence = input('Input lowercase sentence:')
# FIXME: Explicitly specify an encoding
clientSocket.send(sentence.encode())
# FIXME: Check whether we actually received a complete sentence
modifiedSentence = clientSocket.recv(1024)
# FIXME: Explicitly specify the same encoding
print('From Server:', modifiedSentence.decode())
clientSocket.close()
