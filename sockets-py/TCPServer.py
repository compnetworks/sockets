import socket

serverPort = 6789
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)
print('The server is ready to receive')
while True:
    connectionSocket, addr = serverSocket.accept()
    # FIXME: 1) Should explicitly specify the encoding.
    # 2) Should check for whether we actually read a complete sentence
    # because recv(.) may read any number of bytes up to the size argument.
    sentence = connectionSocket.recv(1024).decode()
    capitalizedSentence = sentence.upper()
    # FIXME: Should specify the same encoding and use a method guaranteed to
    #  send all bytes in the argument
    connectionSocket.send(capitalizedSentence.encode())
    connectionSocket.close()
