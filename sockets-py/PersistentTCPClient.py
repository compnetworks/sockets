import socket
import time  # Import necessary modules for socket communication and time measurement

# Define server address and port
server_address = 'localhost'
server_port = 6789

try:
    # Create a socket
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Connect to the server at the specified address and port
    client_socket.connect((server_address, server_port))
    print("Connected to the server.")

    while True:
        # Get the input
        sentence = input("Enter message ('.' to quit): ")
        
        # Send the input message to the server
        client_socket.sendall((sentence + '\n').encode('utf-8'))

        # If the user wants to quit
        if sentence == ".":
            break

        # Record the time immediately after sending the message
        start_time = time.time()

        # FIXME: Should check that we receive a complete sentence
        # Receive the modified sentence from the server up to 1024 bytes
        modified_sentence = client_socket.recv(1024).decode('utf-8')
        
        # Calculate the round-trip time delay by measuring time since sending
        delay = time.time() - start_time
        
        print(f"FROM SERVER: {modified_sentence} delay = {delay:.2f} seconds")

except Exception as e:
    print(f"An error occurred: {e}")

# Closing the socket (though not implemented in the original code)
client_socket.close()