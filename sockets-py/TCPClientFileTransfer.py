import socket

# Create a TCP/IP socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the server
server_address = ('localhost', 6789)
client_socket.connect(server_address)

# Read the bytes of self file
file_path = '/Users/arun/Dropbox/453/sockets/sockets-py/TCPClientFileTransfer.py'
with open(file_path, 'rb') as file:
    bytes = file.read()

print(len(bytes))

# Send the file bytes to the server
client_socket.sendall(bytes)

# Close the socket
client_socket.close()
