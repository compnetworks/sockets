import socket

# Read a stream of bytes of unknown length until EOF is reached.

# Define the buffer size
buffer_size = 16

# Create a TCP/IP socket and bind it to the port
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('localhost', 6789))
server_socket.listen(1)

try:
    while True:
        # Accept a connection from a client
        connection_socket, client_address = server_socket.accept()
        
        with connection_socket:
            total_bytes_read = 0
            while True:
                # Read data from the socket
                data = connection_socket.recv(buffer_size)
                # if there is no data received, close the connection socket
                if not data:
                    break
                # keep track of total bytes read from the socket
                total_bytes_read += len(data)
                print("Read ", len(data), " bytes: ", data.decode())
            
            print("Total bytes read =", total_bytes_read)

finally:
    server_socket.close()
