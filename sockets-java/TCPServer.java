import java.io.*;
import java.net.*;

class TCPServer {

public static void main(String argv[]) throws Exception {
	String clientSentence;
	String capitalizedSentence;

	ServerSocket welcomeSocket = new ServerSocket(6789);

	while (true) {
		Socket connectionSocket = welcomeSocket.accept();
		InputStream inFromClient = connectionSocket.getInputStream();
		OutputStream outToClient = connectionSocket.getOutputStream();
		byte[] buf = new byte[1024];
		// FIXME: Should check whether we actually read a complete sentence
		//  because read(.) may read any number of bytes up to the size of
		//  the array argument.
		int numRead = inFromClient.read(buf);
		// FIXME: Should explicitly specify an encoding of string to
		//  bytes instead of assuming the platform's default encoding.
		clientSentence = new String(buf, 0, numRead);
		capitalizedSentence = clientSentence.toUpperCase();
		// FIXME: We are again assuming the platform's default encoding.
		outToClient.write(capitalizedSentence.getBytes());
	}
}
}
