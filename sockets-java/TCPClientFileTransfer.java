import java.io.*;
import java.net.*;
import java.util.Scanner;

class TCPClientFileTransfer {

public static void main(String args[]) throws Exception {
	// FIXME: Change default path to a valid file path on your machine
	String filename = args.length > 0 ? args[1] : "/Users/arun/workspace" +
			"/sockets/src/TCPClientFileTransfer.java";
	Socket clientSocket = new Socket("localhost", 6789);
	DataOutputStream outToServer = new DataOutputStream(
			clientSocket.getOutputStream());

	String content = new Scanner(new File(filename)).useDelimiter("\\Z").next();
	System.out.println(content.length());

	outToServer.writeBytes(content);
	clientSocket.close();
}
}
