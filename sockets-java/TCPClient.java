import java.io.*;
import java.net.*;

class TCPClient {

public static void main(String argv[]) throws Exception {
	String sentence;
	String modifiedSentence;

	BufferedReader inFromUser = new BufferedReader(
			new InputStreamReader(System.in));
	Socket clientSocket = new Socket("localhost", 6789);
	OutputStream outToServer = clientSocket.getOutputStream();
	InputStream inFromServer = clientSocket.getInputStream();

	sentence = inFromUser.readLine();
	// FIXME: Should specify an encoding from string to bytes
	outToServer.write((sentence + '\n').getBytes());

	byte[] buf = new byte[1024];
	// FIXME: Should check whether we actually read a complete sentence
	// because read(.) may read any number of bytes up to the size of the
	// array argument.
	int numRead = inFromServer.read(buf);
	// FIXME: Should specify the same encoding
	modifiedSentence = new String(buf, 0, numRead);
	System.out.print("FROM SERVER: " + modifiedSentence);
	clientSocket.close();
}
}
