import java.io.*;
import java.net.Socket;

/**
 * @author V. Arun
 */
/* This class simply extends TCPClient to send an arbitrary
 * number of requests. It is a client that will work with
 * all of the following:
 *   PersistentTCPServer
 *   ThreadedPersistentTCPServer
 *   ThreadedPersistentTCPServerWithStats
 */
public class PersistentTCPClient {

/**
 * @param args
 */
public static void main(String[] args) {
	try {
		String sentence;
		String modifiedSentence;
		BufferedReader inFromUser = new BufferedReader(
				new InputStreamReader(System.in));
		Socket clientSocket = new Socket("localhost", 6789);
		OutputStream outToServer = clientSocket.getOutputStream();
		InputStream inFromServer = clientSocket.getInputStream();

		/* This part is the main difference from TCPClient in that
		 * it sends an arbitrary number of requests, not just one,
		 * until the escape sequence is encountered.
		 */
		while (true) {
			sentence = inFromUser.readLine();
			outToServer.write((sentence + '\n').getBytes());
			long t = System.currentTimeMillis();
			if (sentence.equals(".")) break;
			byte[] buf = new byte[1024];
			int numRead = inFromServer.read(buf);
			modifiedSentence = new String(buf, 0, numRead);
			System.out.println(
					"FROM SERVER: " + modifiedSentence + " delay = " + (System.currentTimeMillis() - t) + "ms");
		}
		clientSocket.close();
	} catch (IOException ioe) {
		ioe.printStackTrace();
	}
}
}
