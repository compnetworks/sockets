import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author V. Arun
 */

/* This class simply extends PersistentTCPServer to add support
 * for concurrent clients. There is no actual shared data
 * structures between multiple threads, so there is no
 * concurrency control problems to worry about in this example.
 * We simply added a private class SingleClientHandler that
 * simply invokes handleSingleClient to accomplish the goal.
 */
public class ThreadedPersistentTCPServer {

private class SingleClientHandler implements Runnable {

	private final Socket connectionSocket;

	SingleClientHandler(Socket connectionSocket) {
		this.connectionSocket = connectionSocket;
	}

	@Override
	public void run() {
		try {
			/* In general, you may not be able to call handleSingleClient
			 * directly if this private class, SingleClientHandler, were
			 * in its own file. You need to pass it more arguments in
			 * addition to connectionSocket for that purpose.
			 * In general, it is a good idea to define a class in its
			 * own separate file unless it is a really small class
			 * that is used only (private-ly) inside a single file.
			 */
			handleSingleClient(connectionSocket);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				this.connectionSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
				// give up trying to close the socket at this point
			}
		}
	}
}

// Same as PersistentTCPServer.handleSingleClient(.)
private void handleSingleClient(Socket connectionSocket) throws IOException {
	InputStream inFromClient = connectionSocket.getInputStream();
	OutputStream outToClient = connectionSocket.getOutputStream();

	while (!connectionSocket.isClosed()) {
		byte[] buf = new byte[1024];
		int numRead = inFromClient.read(buf);
		if (numRead > 0) {
			// FIXME: Should check that we receive a complete sentence
			String clientSentence = new String(buf, 0, numRead);
			System.out.print("Received: " + clientSentence);
			if (clientSentence.equals(".")) break;
			String capitalizedSentence = clientSentence.toUpperCase();
			outToClient.write(capitalizedSentence.getBytes());
		}
		else break;
	}
	System.out.println(
			"Closing client connection from " + connectionSocket.getRemoteSocketAddress());
	connectionSocket.close();
}

private void startServer() throws IOException {
	ServerSocket welcomeSocket = new ServerSocket(6789);

	while (true) {
		try {
			Socket connectionSocket = welcomeSocket.accept();
			// this.handleSingleClient(connectionSocket);
			(new Thread(new SingleClientHandler(connectionSocket))).start();
		} catch (IOException e) {
			welcomeSocket.close();
		}
	}
}

/**
 * @param args
 */
public static void main(String[] args) {
	ThreadedPersistentTCPServer server = new ThreadedPersistentTCPServer();
	try {
		server.startServer();
	} catch (IOException e) {
		e.printStackTrace();
	}
}

}
