import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
@author V. Arun
 */
public class PersistentTCPServer {

	/* Similar to TCPServer but with the difference that we need to handle
	 * possibly an arbitrary number of requests from each client.
	 */
	private void handleSingleClient(Socket connectionSocket) throws IOException {
		InputStream inFromClient = connectionSocket.getInputStream();
		OutputStream outToClient = connectionSocket.getOutputStream();

		while(!connectionSocket.isClosed()) {
			byte[] buf = new byte[1024];
			int numRead = inFromClient.read(buf);
			if(numRead > 0)  {
				// FIXME: Should check that we receive a complete sentence
				String clientSentence = new String(buf, 0, numRead);
				System.out.print("Received: " + clientSentence);
				if(clientSentence.equals(".")) break;
				String capitalizedSentence = clientSentence.toUpperCase();
				outToClient.write(capitalizedSentence.getBytes());
			} 
			else break;
		}		
		System.out.println("Closing client connection from " + 
				connectionSocket.getRemoteSocketAddress());
		if(!connectionSocket.isClosed()) connectionSocket.close();        		
	}
	private void startServer() throws IOException {
		ServerSocket welcomeSocket = new ServerSocket(6789);

		while(true)
		{
			Socket connectionSocket = null;
			try {
				connectionSocket = welcomeSocket.accept();
			} catch(IOException e) {
				welcomeSocket.close();
				break;
			}
			try {
				this.handleSingleClient(connectionSocket);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} finally {
				// Always try your best to close everything
				if(!connectionSocket.isClosed()) connectionSocket.close();        		
			}
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PersistentTCPServer server = new PersistentTCPServer();
		try {
			server.startServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
